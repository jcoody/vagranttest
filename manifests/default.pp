exec { "apt-get update":
	path => "/usr/bin"
}
exec { "lamp-stack":
	command => "apt-get install lamp-server^",
	path => "/usr/bin",
	require => Exec["apt-get update"]
}
package { ['curl', 'vim', 'make']:
	ensure => present,
	require => Exec["apt-get update"]
}
package { 'ruby':
	ensure => "1.9.3",
	require => Exec["apt-get update"]
}
file { "/var/www":
	ensure => "link",
	target => "/vagrant",
	force => true
}