#!/usr/bin/env bash

apt-get update
sudo apt-get install -y python-software-properties python curl make build-essential git-core
sudo add-apt-repository ppa:chris-lea/node.js
sudo apt-get update
sudo apt-get install -y nodejs
\curl -sSL https://get.rvm.io | bash -s stable
source /etc/profile.d/rvm.sh
rvm install 1.9.3
rvm use 1.9.3
gem install bundler
cd /vagrant
bundle init
echo "group :development do" >> Gemfile
echo "  gem 'guard'" >> Gemfile
echo "  gem 'guard-sass', :require => false" >> Gemfile
echo "  gem 'guard-livereload', :require => false" >> Gemfile
echo "end" >> Gemfile
bundle
guard init

# INSTALL APACHE PHP5 AND MYSQL
sudo apt-get install -y apache2
rm -rf /var/www
ln -fs /vagrant /var/www
sudo apt-get install -y php5 libapache2-mod-php5 php5-mysql
sudo service apache2 restart
cd /vagrant
touch info.php
echo "<?php" >> info.php
echo "	phpinfo();" >> info.php
echo "?>" >> info.php

#sudo apt-get install -y mysql-server mysql-client